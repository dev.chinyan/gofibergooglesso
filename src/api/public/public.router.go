package public

import (
	"github.com/gofiber/fiber/v2"
)

func PublicRouter(app *fiber.App) {
	app.Get("/", HealthCheck)
}
