package public

import (
	"github.com/gofiber/fiber/v2"
)

func HealthCheck(ctx *fiber.Ctx) error {
	return ctx.JSON(map[string]interface{}{
		"status":  "ok",
		"message": "up and running",
	})
}
