package task_test

import (
	server "TaskManager/src"
	"bytes"
	"encoding/json"

	"TaskManager/src/config"
	ColorLog "TaskManager/src/helper/colorlog"
	MockLogin "TaskManager/src/helper/mockLogin"
	Model "TaskManager/src/models"

	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gofiber/fiber/v2"
	"gopkg.in/go-playground/assert.v1"
)

var (
	App       *fiber.App
	Env       config.Configuration
	UserToken string
)

const PROFILE_ID = "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9"

func TestMain(m *testing.M) {
	Env = config.GetEnv()
	ColorLog.Yellow("Env : ", Env.ENV)
	testServer := &server.Server{
		Host:       Env.HOST,
		Port:       Env.PORT,
		ProjectEnv: Env.ENV,
		Engine: fiber.New(fiber.Config{
			Prefork:       true,
			CaseSensitive: true,
			StrictRouting: true,
			ServerHeader:  "Fiber",
			AppName:       "Task Management App v1.0.1",
		}),
	}
	testServer.Setup()
	App = testServer.GetServerEngine()
	Token, err := MockLogin.GetTokenByProfileID(PROFILE_ID)
	if err != nil {
		ColorLog.Red("\n Error :::> ", err.Error(), "\n")
	}
	UserToken = Token
	ColorLog.Yellow("#############################################")
	ColorLog.Yellow("Test Task Module")
	ColorLog.Yellow("#############################################")
	ColorLog.Cyan("AccessToken : " + UserToken)
	exitVal := m.Run()
	defer os.Exit(exitVal)
}

func Test_Create_Task(t *testing.T) {
	var createTaskResponse Model.ResponseSuccess[Model.Task]

	payload := map[string]any{
		"profileID":       PROFILE_ID,
		"taskName":        "Create a Login Using Google SSO and Oauth2",
		"taskDescription": "Integrate Google Single Sign On with our backend to protect our endpoint",
		"progressStatus":  "IN_PROGRESS",
		"createdBy":       PROFILE_ID,
	}

	buffer, _ := json.Marshal(payload)

	req := httptest.NewRequest(http.MethodPost, "/task/create", bytes.NewBuffer(buffer))
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &createTaskResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, createTaskResponse.Data, nil)
}

func Test_Fetch_Task(t *testing.T) {
	var fetchTaskResponse Model.ResponseSuccess[Model.Task]

	taskID := "tasks/84108c7d-a85f-4f4d-add2-011b44c4edd1"

	req := httptest.NewRequest(http.MethodGet, "/task/fetch?taskID="+taskID, nil)
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &fetchTaskResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, fetchTaskResponse.Data, nil)
}

func Test_List_Task(t *testing.T) {
	var listTaskResponse Model.ResponseSuccess[[]Model.Task]

	profileID := PROFILE_ID
	offset := "0"
	limit := "10"

	req := httptest.NewRequest(http.MethodGet, "/task/list?profileID="+profileID+"&offset="+offset+"&limit="+limit, nil)
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &listTaskResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, listTaskResponse.Data, nil)
}

func Test_Update_Task(t *testing.T) {
	var updateTaskResponse Model.ResponseSuccess[Model.Task]

	payload := map[string]any{
		"_id":             "tasks/84108c7d-a85f-4f4d-add2-011b44c4edd1",
		"_key":            "84108c7d-a85f-4f4d-add2-011b44c4edd1",
		"profileID":       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
		"taskName":        "Expose protected endpoints to perform CRUD on Tasks",
		"taskDescription": "After Integrate Google Single Sign On Oauth2, create protected endpoints for the CRUD of tasks",
		"progressStatus":  "PENDING",
		"active":          true,
		"createdAt":       "2023-10-29T19:56:46+08:00",
		"createdBy":       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
		"updatedAt":       "2023-10-29T19:56:46+08:00",
		"updatedBy":       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
	}

	buffer, _ := json.Marshal(payload)

	req := httptest.NewRequest(http.MethodPut, "/task/update", bytes.NewBuffer(buffer))
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &updateTaskResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, updateTaskResponse.Data, nil)
}

func Test_Update_Task_Status(t *testing.T) {
	var updateTaskStatusResponse Model.ResponseSuccess[Model.Task]

	payload := map[string]any{
		"_id":            "tasks/84108c7d-a85f-4f4d-add2-011b44c4edd1",
		"progressStatus": "IN_PROGRESS",
	}

	buffer, _ := json.Marshal(payload)

	req := httptest.NewRequest(http.MethodPut, "/task/update", bytes.NewBuffer(buffer))
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &updateTaskStatusResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, updateTaskStatusResponse.Data, nil)
}

func Test_Delete_Task(t *testing.T) {
	var deleteTaskResponse Model.ResponseSuccess[Model.Task]

	payload := map[string]any{
		"_id": "tasks/84108c7d-a85f-4f4d-add2-011b44c4edd1",
	}

	buffer, _ := json.Marshal(payload)

	req := httptest.NewRequest(http.MethodDelete, "/task/delete", bytes.NewBuffer(buffer))
	req.Header.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	req.Header.Set(fiber.HeaderAuthorization, "Bearer "+UserToken)

	// Assertions
	res, err := App.Test(req)
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err = json.Unmarshal([]byte(jsonStr), &deleteTaskResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, err, nil)
	assert.NotEqual(t, deleteTaskResponse.Data, nil)
}
