package task

type createTask struct {
	ProfileID string `json:"profileID" validate:"required,profileID"`

	TaskName        string `json:"taskName" validate:"required"`
	TaskDescription string `json:"taskDescription" validate:"required"`
	ProgressStatus  string `json:"progressStatus" validate:"required,oneof=PENDING IN_PROGRESS BLOCKED COMPLETED CANCELLED"`
	CreatedBy       string `json:"createdBy" validate:"required,profileID"`
}

type fetchTask struct {
	TaskID string `json:"taskID" validate:"required,taskID"`
}

type listTask struct {
	ProfileID string `json:"profileID" validate:"omitempty,profileID"`
	CreatedBy string `json:"createdBy" validate:"omitempty,profileID"`
	Offset    int    `json:"offset" validate:"omitempty"`
	Limit     int    `json:"limit" validate:"required"`
}

type updateTask struct {
	Id  string `json:"_id" validate:"required,taskID"`
	Key string `json:"_key" validate:"required,uuid4_rfc4122"`

	ProfileID string `json:"profileID" validate:"required,profileID"`

	TaskName        string `json:"taskName" validate:"required"`
	TaskDescription string `json:"taskDescription" validate:"required"`
	ProgressStatus  string `json:"progressStatus" validate:"required,oneof=PENDING IN_PROGRESS BLOCKED COMPLETED CANCELLED"`

	Active    bool   `json:"active" validate:"required"`
	CreatedAt string `json:"createdAt" validate:"required"`
	CreatedBy string `json:"createdBy" validate:"required,profileID"`
	UpdatedAt string `json:"updatedAt" validate:"required"`
	UpdatedBy string `json:"updatedBy" validate:"required,profileID"`
}

type updateTaskStatus struct {
	Id             string `json:"_id" validate:"required,taskID"`
	ProgressStatus string `json:"progressStatus" validate:"required,oneof=PENDING IN_PROGRESS BLOCKED COMPLETED CANCELLED"`
}

type deleteTask struct {
	Id string `json:"_id" validate:"required,taskID"`
}
