package task

import (
	crypto "TaskManager/src/helper/cryptography"
	PL "TaskManager/src/helper/payloadValidator"

	"github.com/gofiber/fiber/v2"
)

func TaskRouter(app *fiber.App) {
	taskRouter := app.Group("task")

	//Middleware
	taskRouter.Use(crypto.ValidateJwtMiddleware)

	//router
	taskRouter.Post("/create", PL.ValidatePayload(&createTask{}), createTaskHandler)
	taskRouter.Get("/fetch", PL.ValidatePayload(&fetchTask{}), fetchTaskHandler)
	taskRouter.Get("/list", PL.ValidatePayload(&listTask{}), listTaskHandler)
	taskRouter.Put("/update", PL.ValidatePayload(&updateTask{}), updateTaskHandler)
	taskRouter.Put("/status/update", PL.ValidatePayload(&updateTaskStatus{}), updateTaskStatusHandler)
	taskRouter.Delete("/delete", PL.ValidatePayload(&deleteTask{}), deleteTaskHandler)
}
