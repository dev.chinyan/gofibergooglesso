package task

import (
	"TaskManager/src/config"
	docHelper "TaskManager/src/helper/document"
	"TaskManager/src/helper/response"
	Model "TaskManager/src/models"
	repo "TaskManager/src/repository"

	"github.com/gofiber/fiber/v2"
)

func createTaskHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*createTask)
	creatorProfileID := payload.CreatedBy

	task, err := docHelper.AddBaseField[*Model.Task](payload, config.TASK_COLLECTION, creatorProfileID, nil)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("add base fields failed", err))
	}

	err = repo.TaskRepo.Insert(task)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("Insert Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("Insert Task Successfully", task))
}

func fetchTaskHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*fetchTask)
	TaskID := payload.TaskID

	fetchedTask, err := repo.TaskRepo.Fetch(TaskID, &Model.Task{})
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("Fetch Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("Fetch Task Successfully", fetchedTask.(*Model.Task)))
}

func listTaskHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*listTask)

	tasks, err := repo.TaskRepo.List(
		payload.ProfileID,
		payload.CreatedBy,
		payload.Offset,
		payload.Limit,
	)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("List Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("List Task Successfully", tasks))
}

func updateTaskHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*updateTask)
	taskID := payload.Id
	payloadMap := ctx.Locals("payloadMap").(map[string]any)

	err := repo.TaskRepo.Update(payloadMap, taskID)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("Update Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("Update Task Successfully", payload))
}

func updateTaskStatusHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*updateTaskStatus)
	taskID := payload.Id

	err := repo.TaskRepo.Update(fiber.Map{"progressStatus": payload.ProgressStatus}, taskID)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("Update Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("update Task Successfully", payload))
}

func deleteTaskHandler(ctx *fiber.Ctx) error {
	payload := ctx.Locals("validatedPayload").(*deleteTask)
	taskID := payload.Id

	err := repo.TaskRepo.Update(fiber.Map{"active": false}, taskID)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("Delete Task Failed", err))
	}

	ctx.Status(fiber.StatusOK)
	return ctx.JSON(response.Success("Delete Task Successfully", payload))
}
