package auth

import (
	"github.com/gofiber/fiber/v2"
)

func AuthRouter(app *fiber.App) {
	authRouter := app.Group("auth")
	authRouter.Get("/google/login", googleLogin)
	authRouter.Get("/google/callback", googleCallback)
	// authRouter.Post("/google/callback", googleCallback)
}
