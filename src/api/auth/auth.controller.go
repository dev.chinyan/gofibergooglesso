package auth

import (
	"TaskManager/src/config"
	ColorLog "TaskManager/src/helper/colorlog"
	docHelper "TaskManager/src/helper/document"
	GoogleOAuth2 "TaskManager/src/helper/googleOAuth2"
	"context"

	crypto "TaskManager/src/helper/cryptography"
	"TaskManager/src/helper/response"
	Model "TaskManager/src/models"
	repo "TaskManager/src/repository"

	"github.com/gofiber/fiber/v2"
)

func googleLogin(ctx *fiber.Ctx) error {
	googleConfig := config.SetupGoogleOauth2Config()
	url := googleConfig.AuthCodeURL("randomSecret")

	ColorLog.Yellow("url : ", url)

	ctx.Redirect(url, fiber.StatusSeeOther)
	return ctx.JSON(map[string]interface{}{
		"status":  "ok",
		"message": "up and running",
	})
}

func googleCallback(ctx *fiber.Ctx) error {
	state := ctx.Query("state")
	if state != "randomSecret" {
		ColorLog.Red(ctx.Response(), "state doesn't match")
		// return
	}

	code := ctx.Query("code")
	googleConfig := config.SetupGoogleOauth2Config()

	token, err := googleConfig.Exchange(context.Background(), code)
	if err != nil || token == nil {
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(response.Unauthorized("code-token exchange failed", nil))
	}

	googleProfileData, err := GoogleOAuth2.VerifyToken(token.AccessToken)
	if err != nil {
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(response.Unauthorized("Invalid Token", err))
	}

	ColorLog.Cyan(googleProfileData)
	fetchedProfile, err := repo.ProfileRepo.FindByGoogleProfileID(googleProfileData["id"].(string))
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		return ctx.JSON(response.InternalServerError("fetch profile failed", err))
	}
	if len(fetchedProfile) == 0 {
		newProfileKey := docHelper.UUID()
		newProfileID := config.PROFILE_COLLECTION + "/" + newProfileKey
		profile, err := docHelper.AddBaseField[*Model.Profile](&Model.Profile{
			GoogleProfileID: googleProfileData["id"].(string),
			Email:           googleProfileData["email"].(string),
			FamilyName:      googleProfileData["family_name"].(string),
			GivenName:       googleProfileData["given_name"].(string),
		}, config.PROFILE_COLLECTION, newProfileID, newProfileKey)
		if err != nil {
			ctx.Status(fiber.StatusInternalServerError)
			ctx.JSON(response.InternalServerError("add base field failed", err))
		}
		err = repo.ProfileRepo.Insert(profile)
		if err != nil {
			ctx.Status(fiber.StatusInternalServerError)
			ctx.JSON(response.InternalServerError("create profile failed", err))
		}

		fetchedProfile[0] = profile
	}

	AccessToken, err := crypto.GenerateJWT(config.ENV.JWT_SECRET, docHelper.DocToMap(fetchedProfile[0]))
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError)
		ctx.JSON(response.InternalServerError("create token failed", err))
	}

	return ctx.JSON(response.Success("Login successfully", fiber.Map{
		"profile":     fetchedProfile[0],
		"AccessToken": AccessToken,
	}))
}
