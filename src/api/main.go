package api

import (
	"TaskManager/src/api/auth"
	"TaskManager/src/api/public"
	"TaskManager/src/api/task"

	"github.com/gofiber/fiber/v2"
)

func MainRouters(app *fiber.App) {
	public.PublicRouter(app)
	auth.AuthRouter(app)
	task.TaskRouter(app)
}
