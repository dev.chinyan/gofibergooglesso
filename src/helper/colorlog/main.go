package ColorLog

import (
	"fmt"
)

const (
	Reset      = "\x1b[0m"
	Bright     = "\x1b[1m"
	Dim        = "\x1b[2m"
	Underscore = "\x1b[4m"
	Blink      = "\x1b[5m"
	Reverse    = "\x1b[7m"
	Hidden     = "\x1b[8m"

	FgBlack   = "\x1b[30m"
	FgRed     = "\x1b[31m"
	FgGreen   = "\x1b[32m"
	FgYellow  = "\x1b[33m"
	FgBlue    = "\x1b[34m"
	FgMagenta = "\x1b[35m"
	FgCyan    = "\x1b[36m"
	FgWhite   = "\x1b[37m"
	FgGray    = "\x1b[90m"

	BgBlack   = "\x1b[40m"
	BgRed     = "\x1b[41m"
	BgGreen   = "\x1b[42m"
	BgYellow  = "\x1b[43m"
	BgBlue    = "\x1b[44m"
	BgMagenta = "\x1b[45m"
	BgCyan    = "\x1b[46m"
	BgWhite   = "\x1b[47m"
	BgGray    = "\x1b[100m"
)

func Red(input ...interface{}) {
	fmt.Print(FgRed)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Green(input ...interface{}) {
	fmt.Print(FgGreen)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Cyan(input ...interface{}) {
	fmt.Print(FgCyan)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Blue(input ...interface{}) {
	fmt.Print(FgBlue)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Magenta(input ...interface{}) {
	fmt.Print(FgMagenta)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Yellow(input ...interface{}) {
	fmt.Print(FgYellow)
	fmt.Println(input...)
	fmt.Print(Reset)
}

func Gray(input ...interface{}) {
	fmt.Print(FgGray)
	fmt.Println(input...)
	fmt.Print(Reset)
}
