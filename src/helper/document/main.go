package docHelper

import (
	"strings"
	"time"

	"github.com/fatih/structs"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
)

func AddBaseField[T interface{}](document interface{}, collectionName string, creatorProfileID string, key interface{}) (T, error) {
	Map := structs.Map(document)
	var UUID string
	if key == nil {
		UUID = uuid.New().String()
	} else {
		UUID = key.(string)
	}
	timestamp := time.Now().Format(time.RFC3339)
	Map["Key"] = UUID
	Map["Id"] = collectionName + "/" + UUID
	Map["Active"] = true
	Map["CreatedAt"] = timestamp
	Map["CreatedBy"] = creatorProfileID
	Map["UpdatedAt"] = timestamp
	Map["UpdatedBy"] = creatorProfileID

	var Struct T
	err := mapstructure.Decode(Map, &Struct)
	if err != nil {
		return Struct, err
	}
	return Struct, nil
}

func UUID() string {
	return uuid.New().String()
}

func DocToMap(doc interface{}) map[string]interface{} {
	var Map = make(map[string]interface{})
	TempMap := structs.Map(doc)
	for Key, Value := range TempMap {
		if Key == "Id" {
			NewKey := "_id"
			Map[NewKey] = Value
		} else if Key == "Key" {
			NewKey := "_key"
			Map[NewKey] = Value
		} else if Key == "Rev" {
			NewKey := "_rev"
			Map[NewKey] = Value
		} else if Key == "From" {
			NewKey := "_from"
			Map[NewKey] = Value
		} else if Key == "To" {
			NewKey := "_to"
			Map[NewKey] = Value
		} else {
			firstChar := string(Key[0])
			NewKey := strings.ToLower(firstChar) + Key[1:]
			Map[NewKey] = Value
		}
	}
	return Map
}

func MapToDoc[T any](KeyValue map[string]any) (T, error) {
	var Struct T
	KeyValue["Id"] = KeyValue["_id"]
	KeyValue["Key"] = KeyValue["_key"]
	KeyValue["From"] = KeyValue["_from"]
	KeyValue["To"] = KeyValue["_to"]
	err := mapstructure.Decode(KeyValue, &Struct)
	if err != nil {
		return Struct, err
	}
	return Struct, nil
}

func ParseDocs[T any](arrMaps []map[string]any) ([]T, error) {
	arr := make([]T, 0)
	for _, value := range arrMaps {
		doc, err := MapToDoc[T](value)
		if err != nil {
			return arr, err
		}
		arr = append(arr, doc)
	}
	return arr, nil
}
