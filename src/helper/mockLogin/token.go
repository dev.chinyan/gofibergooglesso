package MockLogin

import (
	"TaskManager/src/config"
	crypto "TaskManager/src/helper/cryptography"
	docHelper "TaskManager/src/helper/document"
	Model "TaskManager/src/models"
	repo "TaskManager/src/repository"
)

func GetTokenByProfileID(profileID string) (string, error) {
	profile, err := repo.ProfileRepo.Fetch(profileID, &Model.Profile{})
	if err != nil {
		return err.Error(), err
	}

	profile = profile.(*Model.Profile)
	AccessToken, err := crypto.GenerateJWT(config.ENV.JWT_SECRET, docHelper.DocToMap(profile))
	if err != nil {
		return err.Error(), err
	}

	return AccessToken, nil
}
