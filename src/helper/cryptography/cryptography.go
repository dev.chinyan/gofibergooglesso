package cryptography

import (
	config "TaskManager/src/config"
	response "TaskManager/src/helper/response"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"

	ColorLog "TaskManager/src/helper/colorlog"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
)

type (
	StoredPassword struct {
		HashedPassword string
		Salt           string
	}
)

var JWTSignature string = config.ENV.JWT_SECRET

func EncodeBase64(data string) string {
	base64 := b64.StdEncoding.EncodeToString([]byte(data))
	return base64
}

func DecodeBase64(base64 string) string {
	data, _ := b64.StdEncoding.DecodeString(base64)
	return string(data)
}

func RandomNumb() int {
	min := 1000000000000000
	max := 9999999999999999
	rand := rand.Intn(max-min+1) + min
	return rand
}

func HashPassword(password string) (error, string) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err, ""
	}
	password = string(bytes)

	return nil, password

}

func HashPasswordWithSalt(password string) (string, string) {
	salt := fmt.Sprint(RandomNumb())
	bytes, err := bcrypt.GenerateFromPassword([]byte(password+salt), bcrypt.DefaultCost)
	if err != nil {
		return err.Error(), err.Error()
	}
	password = string(bytes)

	return salt, password

}
func CheckPasswordWithSalt(inputPassword string, HashedPassword string, Salt string) (error, bool) {
	salt := Salt
	authErr := bcrypt.CompareHashAndPassword([]byte(HashedPassword), []byte(inputPassword+salt))
	if authErr != nil {
		return authErr, false
	}
	return nil, true
}

func CheckPassword(inputPassword string, HashedPassword string) (error, bool) {
	authErr := bcrypt.CompareHashAndPassword([]byte(HashedPassword), []byte(inputPassword))
	if authErr != nil {
		return authErr, false
	}
	return nil, true
}

func GenerateRandomPassword() RandomPassword {
	randPswd := strconv.Itoa(RandomNumb())
	randPswd = EncodeBase64(randPswd)

	salt, hash := HashPasswordWithSalt(randPswd)

	return RandomPassword{
		Pswd: randPswd,
		Hash: hash,
		Salt: salt,
	}

}

func UUID() uuid.UUID {
	return uuid.New()
}

func GenerateJWT(signature string, payload map[string]interface{}) (string, error) {
	var mySigningKey = []byte(signature)
	token := jwt.New(jwt.SigningMethodHS256)
	JwtData := token.Claims.(jwt.MapClaims)

	JwtData["authorized"] = true
	// JwtData["orgID"] = payload["orgID"]
	// JwtData["role"] = payload["role"]
	// JwtData["username"] = payload["username"]
	for key, value := range payload {
		JwtData[key] = value
	}
	if strings.ToLower(config.ENV.ENV) == "test" {
		JwtData["exp"] = time.Now().Add(time.Minute * 60 * 24 * 30 * 12 * 100).Unix() //expire 100 years later for test
	} else {
		JwtData["exp"] = time.Now().Add(time.Minute * 60 * 2).Unix()
	}

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		ColorLog.Red("Sign Token Error : " + err.Error())
		return "", err
	}
	return tokenString, nil
}

func ValidateJwtMiddleware(ctx *fiber.Ctx) error {
	header := ctx.GetReqHeaders()
	authHeader := header["Authorization"][0]
	if len(authHeader) == 0 {
		err := "Authorization token required"
		errRes := response.Unauthorized(err, nil)
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(errRes)
	}

	authHeaderSplit := strings.Split(authHeader, " ")
	if len(authHeaderSplit) < 2 {
		err := "Invalid token format"
		errRes := response.Unauthorized(err, nil)
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(errRes)
	}
	ColorLog.Yellow("authHeaderSplit : ok")

	authType := authHeaderSplit[0]
	ColorLog.Yellow("authType : ok ")

	token := authHeaderSplit[1]
	if strings.ToLower(authType) != "bearer" {
		err := "Auth type is not Bearer"
		errRes := response.Unauthorized(err, nil)
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(errRes)
	}
	if len(token) == 0 {
		err := "Token is required"
		errRes := response.Unauthorized(err, nil)
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(errRes)
	}
	ColorLog.Yellow("token : ok ")

	var mySigningKey = []byte(JWTSignature)

	jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error in parsing")
		}
		return mySigningKey, nil
	})

	if err != nil {
		err := "Token Malform"
		res := response.Unauthorized(err, errors.New(err))
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(res)
	}

	if jwtData, ok := jwtToken.Claims.(jwt.MapClaims); ok && jwtToken.Valid {
		ColorLog.Blue("JWT Data : ok")
		ColorLog.Yellow(jwtData)

		//Push data to ctx
		ctx.Locals("jwtData", jwtData)

		//pass ctx to next node
		return ctx.Next()

	}

	return nil
}
