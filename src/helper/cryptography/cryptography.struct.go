package cryptography

type (
	JwtData struct {
		Exp        interface{}
		Authorized interface{}
		OrgID      interface{}
		Role       interface{}
		ProfileID  interface{}
	}

	CustomData struct {
		Data map[string]interface{}
	}

	RandomPassword struct {
		Pswd string
		Hash string
		Salt string
	}
)
