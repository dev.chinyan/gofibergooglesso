package GoogleOAuth2

import (
	"TaskManager/src/helper/response"
	"context"
	"encoding/json"
	"errors"
	"strings"

	"TaskManager/src/config"

	"github.com/gofiber/fiber/v2"
	"golang.org/x/oauth2"
)

func GetTokenWithEmailAndPassword(username, password string) (*oauth2.Token, error) {
	googleConfig := config.SetupGoogleOauth2Config()
	Token, err := googleConfig.PasswordCredentialsToken(context.Background(), username, password)
	if err != nil {
		return nil, err
	}
	return Token, nil
}
func VerifyToken(AccessToken string) (map[string]any, error) {
	agent := fiber.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + AccessToken)
	statusCode, body, errs := agent.Bytes()
	if len(errs) > 0 {
		errorStrings := make([]string, len(errs))
		for i, err := range errs {
			errorStrings[i] = err.Error()
		}

		errorMessage := strings.Join(errorStrings, "\n")

		return nil, fiber.NewError(statusCode, errorMessage)
	}

	var tokenInfo fiber.Map
	err := json.Unmarshal(body, &tokenInfo)
	if err != nil {
		return nil, err
	}
	return tokenInfo, nil
}

func GetTokenFromHeader(ctx *fiber.Ctx) (string, error) {
	headers := ctx.GetReqHeaders()
	if len(headers["Authorization"]) == 0 {
		return "", errors.New("Authorization token required")
	}
	authHeader := headers["Authorization"][0]
	authHeaderSplit := strings.Split(authHeader, " ")
	if len(authHeaderSplit) < 2 {
		return "", errors.New("Invalid token format")
	}

	authType := authHeaderSplit[0]

	token := authHeaderSplit[1]
	if strings.ToLower(authType) != "bearer" {
		return "", errors.New("Auth type is not Bearer")
	}
	if len(token) == 0 {
		return "", errors.New("Token is required")
	}

	return token, nil
}

func TokenVerificationMiddleware(ctx *fiber.Ctx) error {
	jwt, parsingErr := GetTokenFromHeader(ctx)
	if parsingErr != nil {
		ctx.Status(fiber.StatusBadRequest)
		return ctx.JSON(response.BadRequest("Unauthorized", parsingErr))
	}
	if len(jwt) == 0 {
		ctx.Status(fiber.StatusBadRequest)
		return ctx.JSON(response.BadRequest("taken required", errors.New("token cannot be nil")))
	}

	jwtClaim, err := VerifyToken(jwt)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest)
		return ctx.JSON(response.BadRequest("Invalid Token", errors.New("Invalid Token")))
	}
	ctx.Locals("jwtClaim", jwtClaim)
	return ctx.Next()
}
