package PayloadValidator

import (
	"regexp"

	"TaskManager/src/config"

	"github.com/go-playground/validator/v10"
)

type (
	ValidatorMatrix struct {
		Tag    string
		Regexp string
	}
)

func RegexpValidator(fl validator.FieldLevel, regexStr string) bool {

	str := fl.Field().String()
	re := regexp.MustCompile(regexStr)
	return re.MatchString(str)
}

const (
	// "(?i)^([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$"
	// "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"
	//" (?i)[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"
	UuidV4Regex      string = "([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89aAbB][a-fA-F0-9]{3}-[a-fA-F0-9]{12})$"
	ProfileID_Regexp string = config.PROFILE_COLLECTION + "/" + UuidV4Regex
	TaskID_Regexp    string = config.TASK_COLLECTION + "/" + UuidV4Regex
)

var ValidatorMatrices []*ValidatorMatrix = []*ValidatorMatrix{
	{Tag: "uuidv4", Regexp: UuidV4Regex},
	{Tag: "profileID", Regexp: ProfileID_Regexp},
	{Tag: "taskID", Regexp: TaskID_Regexp},
}
