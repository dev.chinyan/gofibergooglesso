package PayloadValidator

import (
	ColorLog "TaskManager/src/helper/colorlog"
	docHelper "TaskManager/src/helper/document"
	"TaskManager/src/helper/response"
	"regexp"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

var PayloadValidator *validator.Validate

func InitValidator() {
	// if PayloadValidator != nil {
	PayloadValidator = validator.New()
	// ColorLog.Cyan("init payload validator")

	// Customized Validator
	for _, validationMatrix := range ValidatorMatrices {
		reg := validationMatrix.Regexp
		PayloadValidator.RegisterValidation(
			validationMatrix.Tag,
			func(fl validator.FieldLevel) bool {
				re := regexp.MustCompile(reg)
				result := re.MatchString(fl.Field().String())
				return result
			},
		)
		ColorLog.Yellow("Registered Regular expression for "+validationMatrix.Tag+" : ", validationMatrix.Regexp)

	}
	// }
}

func GetValidator() *validator.Validate {
	//InitValidator()
	return PayloadValidator
}

func ValidatePayload(schema interface{}) fiber.Handler {
	// ColorLog.Blue("Middleware Triggered")
	return func(ctx *fiber.Ctx) error {

		if errRes := ctx.BodyParser(schema); errRes != nil {
			ColorLog.Magenta("errRes 1 ", errRes, schema)
			if errRes := ctx.QueryParser(schema); errRes != nil {
				ColorLog.Magenta("errRes 2 ", errRes, schema)
				if errRes := ctx.ParamsParser(schema); errRes != nil {
					ColorLog.Magenta("errRes 3 ", errRes)
					ctx.Status(fiber.StatusInternalServerError)
					return ctx.JSON(response.BadRequest(errRes.Error(), errRes))
				}
			}
		}

		ColorLog.Blue("schema ===> ", schema)
		payloadMap := docHelper.DocToMap(schema)
		ColorLog.Blue("payloadMap ===> ", payloadMap)

		payloadValidator := GetValidator()
		// ColorLog.Yellow(payloadValidator)
		if validationError := payloadValidator.Struct(schema); validationError != nil {
			ColorLog.Red(validationError.Error())
			errRes := response.BadRequest(validationError.Error(), validationError)
			ctx.Status(fiber.StatusBadRequest)
			return ctx.JSON(errRes)

		}
		ctx.Locals("validatedPayload", schema)
		ctx.Locals("payloadMap", payloadMap)
		return ctx.Next()
	}
}
