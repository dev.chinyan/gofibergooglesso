package response

import (
	ColorLog "TaskManager/src/helper/colorlog"
	"errors"
)

func Success[T any](msg string, data T) map[string]interface{} {
	return map[string]interface{}{
		"status":    200,
		"success":   true,
		"error":     false,
		"message":   msg,
		"data":      data,
		"errObject": nil,
	}
}

func BadRequest(msg string, data error) map[string]interface{} {
	if data == nil {
		data = errors.New(msg)
	}
	ColorLog.Magenta("Bad Request : ", data.Error())
	ColorLog.Magenta(" message : ", msg)
	return map[string]interface{}{
		"status":    400,
		"success":   false,
		"error":     true,
		"message":   msg,
		"data":      nil,
		"errObject": data.Error(),
	}
}

func Unauthorized(msg string, data error) map[string]interface{} {
	if data == nil {
		data = errors.New(msg)
	}
	ColorLog.Magenta("Unauthorized : ", data.Error())
	ColorLog.Magenta(" message :", msg)
	return map[string]interface{}{
		"status":    401,
		"success":   false,
		"error":     true,
		"message":   msg,
		"data":      nil,
		"errObject": data.Error(),
	}
}

func InternalServerError(msg string, data error) map[string]interface{} {
	if data == nil {
		data = errors.New(msg)
	}
	ColorLog.Red("Internal Server Error : ", data.Error())
	ColorLog.Red("\n message : ", msg)
	return map[string]interface{}{
		"status":    500,
		"success":   false,
		"error":     true,
		"message":   msg,
		"data":      nil,
		"errObject": data.Error(),
	}

}
