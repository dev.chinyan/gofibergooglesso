package Model

type Profile struct {
	Id  string `json:"_id" validate:"omitempty"`
	Key string `json:"_key" validate:"omitempty,uuid4_rfc4122"`

	GoogleProfileID string `json:"googleProfileID" validate:"required"`
	Email           string `json:"email" validate:"required"`
	FamilyName      string `json:"familyName" validate:"required"`
	GivenName       string `json:"givenName" validate:"required"`

	Active    bool   `json:"active" validate:"required"`
	CreatedAt string `json:"createdAt" validate:"omitempty"`
	CreatedBy string `json:"createdBy" validate:"omitempty"`
	UpdatedAt string `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string `json:"updatedBy" validate:"omitempty"`
}
