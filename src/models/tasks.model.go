package Model

type Task struct {
	Id  string `json:"_id" validate:"omitempty"`
	Key string `json:"_key" validate:"omitempty,uuid4_rfc4122"`

	ProfileID string `json:"profileID" validate:"required"`

	TaskName        string `json:"taskName" validate:"required"`
	TaskDescription string `json:"taskDescription" validate:"required"`
	ProgressStatus  string `json:"progressStatus" validate:"required,oneof=PENDING IN_PROGRESS BLOCKED COMPLETED CANCELLED"`
	Active          bool   `json:"active" validate:"required"`

	CreatedAt string `json:"createdAt" validate:"omitempty"`
	CreatedBy string `json:"createdBy" validate:"omitempty"`
	UpdatedAt string `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string `json:"updatedBy" validate:"omitempty"`
}
