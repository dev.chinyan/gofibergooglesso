package Model

type ResponseSuccess[T any] struct {
	Status    int    `json:"status"`
	Success   bool   `json:"success"`
	Error     bool   `json:"error"`
	Message   string `json:"message" `
	Data      T      `json:"data"`
	ErrObject error  `json:"errObject"`
}

type ResponseError struct {
	Status    int    `json:"status"`
	Success   bool   `json:"success"`
	Error     bool   `json:"error"`
	Message   string `json:"message" `
	Data      any    `json:"data"`
	ErrObject error  `json:"errObject"`
}
