package server

import (
	"TaskManager/src/api"
	"TaskManager/src/database/arango"
	PL "TaskManager/src/helper/payloadValidator"

	// "TaskManager/src/middleware/swagger"

	"github.com/gofiber/fiber/v2"
)

type Server struct {
	Host       string
	Port       string
	ProjectEnv string
	Engine     *fiber.App
}

func (s *Server) Setup() {
	app := s.Engine
	arango.Connect()
	PL.InitValidator()
	api.MainRouters(app)
	// swagger.Swagger(app)
	arango.SetupArango()

}

func (s *Server) GetServerEngine() *fiber.App {
	return s.Engine
}

func (s *Server) Start() {
	app := s.Engine
	app.Listen(s.Host + ":" + s.Port)
}
