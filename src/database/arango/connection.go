package arango

import (
	"TaskManager/src/config"
	ColorLog "TaskManager/src/helper/colorlog"

	"crypto/tls"

	"github.com/arangodb/go-driver"
	ArangoDriver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

// Environment Variable
var Env = config.ENV

// Instances
var arangoClient ArangoDriver.Client
var arangoConnection ArangoDriver.Connection
var arangoDB ArangoDriver.Database

func Connect() error {
	_, err := GetConnection()
	if err != nil {
		return err
	}
	_, err = GetClient()
	if err != nil {
		return err
	}

	_, err = GetDatabase()
	if err != nil {
		return err
	} else {
		ColorLog.Green("Connected To ArangoDB : ", Env.ARANGODB_DBNAME)
	}

	return nil
}

// Singleton Functions
func GetConnection() (ArangoDriver.Connection, error) {

	if arangoConnection == nil {
		connectionString := Env.ARANGODB_PROTOCOL + "://" + Env.ARANGODB_HOST + ":" + Env.ARANGODB_PORT
		ColorLog.Cyan("connectionString : ", connectionString)
		conn, err := http.NewConnection(http.ConnectionConfig{
			Endpoints: []string{connectionString},
			TLSConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		})
		if err != nil {
			ColorLog.Red("Arango Connection Error : ", err)
			return nil, err
		}
		arangoConnection = conn
	}
	return arangoConnection, nil
}

func GetClient() (ArangoDriver.Client, error) {
	// ColorLog.Cyan("Getting Arango Client ...")
	if arangoClient == nil {
		// ColorLog.Purple(Env)
		// ColorLog.Purple(map[string]any{"User": Env.ARANGODB_USER, "Password": Env.ARANGODB_PASSWORD})
		conn, _ := GetConnection()
		client, err := ArangoDriver.NewClient(ArangoDriver.ClientConfig{
			Connection:     conn,
			Authentication: driver.BasicAuthentication(Env.ARANGODB_USER, Env.ARANGODB_PASSWORD),
		})
		if err != nil {
			ColorLog.Red("Get Arango Client Error : ", err)
			return nil, err
		}
		arangoClient = client
	}
	return arangoClient, nil
}

func GetDatabase() (ArangoDriver.Database, error) {
	client, err := GetClient()
	if err != nil {
		return nil, err
	}
	dbname := Env.ARANGODB_DBNAME
	if Env.ENV == "test" {
		dbname += "_test"
	}

	if arangoDB == nil {
		// ColorLog.Blue(Env.ARANGODB_DBNAME)
		db_exists, db_error := client.DatabaseExists(nil, dbname)
		if db_error != nil {
			ColorLog.Red("Check DB error :::>", db_error, dbname)
			return nil, db_error
		}
		if db_exists {
			ColorLog.Blue("No db is created, " + dbname + " exists already")

			db, err := client.Database(nil, dbname)
			if err != nil {
				ColorLog.Red("Failed to open existing database:", err)
				return nil, err
			}
			arangoDB = db
		} else {
			ColorLog.Cyan("Creating Arango DB ...")
			db, err := client.CreateDatabase(nil, dbname, nil)
			if err != nil {
				ColorLog.Red("Failed to create database :", err)
				return nil, err
			} else {
				ColorLog.Green(dbname + " Created Successfully")
			}
			arangoDB = db
		}
	}
	return arangoDB, nil
}
