package arango

import (
	"TaskManager/src/config"
	Indexes "TaskManager/src/database/arango/seed/index"
	ColorLog "TaskManager/src/helper/colorlog"
)

func SetupCollection() {
	db, err := GetDatabase()
	if err != nil {
		ColorLog.Red(err)
	}

	for _, val := range config.ARANGO_COLLECTION {
		collection := val
		found, err := db.CollectionExists(nil, collection)
		if err != nil {
			ColorLog.Red(err)
		}
		if !found {
			_, err := db.CreateCollection(nil, collection, nil)
			if err != nil {
				ColorLog.Red(err)
			}
		}

	}

	for _, IndexesSeedMatrix := range Indexes.IndexesSeedMatrices {
		collectonName := IndexesSeedMatrix.CollectionName
		indexFields := IndexesSeedMatrix.IndexFields

		collection, err := db.Collection(nil, collectonName) // Replace with your collection name
		if err != nil {
			ColorLog.Red(err)
			continue
		}

		_, _, err = collection.EnsureHashIndex(nil, indexFields, nil)
		if err != nil {
			ColorLog.Red(err)
			continue
		}

		// ColorLog.Green("Index created with ID: %s\n", index)
	}

}
