package ViewMatrices

import (
	"TaskManager/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

type AliesViewMatrix struct {
	Name      string
	AliesView *ArangoDriver.ArangoSearchViewProperties
}

type ViewMatrix struct {
	Name string
	View *ArangoDriver.ArangoSearchViewProperties
}

var coll = config.ARANGO_COLLECTION

var ViewMatrices = []ViewMatrix{
	*FirstMileTripView,
	*LastMileTripView,
}
