package ViewMatrices

import (
	"TaskManager/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

var FirstMileTripView = &ViewMatrix{
	Name: "profile_view",
	View: &ArangoDriver.ArangoSearchViewProperties{
		Links: ArangoDriver.ArangoSearchLinks{
			config.PROFILE_COLLECTION: ArangoDriver.ArangoSearchElementProperties{
				Analyzers: []string{"identity", "text_en"},
				Fields: ArangoDriver.ArangoSearchFields{
					"_id":             ArangoDriver.ArangoSearchElementProperties{},
					"_key":            ArangoDriver.ArangoSearchElementProperties{},
					"email":           ArangoDriver.ArangoSearchElementProperties{},
					"googleProfileID": ArangoDriver.ArangoSearchElementProperties{},
					"familyName":      ArangoDriver.ArangoSearchElementProperties{},
					"givenName":       ArangoDriver.ArangoSearchElementProperties{},
					"active":          ArangoDriver.ArangoSearchElementProperties{},
					"createdAt":       ArangoDriver.ArangoSearchElementProperties{},
					"createdBy":       ArangoDriver.ArangoSearchElementProperties{},
					"updatedAt":       ArangoDriver.ArangoSearchElementProperties{},
					"updatedBy":       ArangoDriver.ArangoSearchElementProperties{},
				},
			},
		},
		//PrimarySortCompression: "LZ4",
	},
}
