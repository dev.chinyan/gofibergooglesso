package ViewMatrices

import (
	"TaskManager/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

var LastMileTripView = &ViewMatrix{
	Name: "tasks_view",
	View: &ArangoDriver.ArangoSearchViewProperties{
		Links: ArangoDriver.ArangoSearchLinks{
			config.TASK_COLLECTION: ArangoDriver.ArangoSearchElementProperties{
				Analyzers: []string{"identity", "text_en"},
				Fields: ArangoDriver.ArangoSearchFields{
					"_id":             ArangoDriver.ArangoSearchElementProperties{},
					"_key":            ArangoDriver.ArangoSearchElementProperties{},
					"googleProfileID": ArangoDriver.ArangoSearchElementProperties{},
				},
			},
		},
		//PrimarySortCompression: "LZ4",
	},
}
