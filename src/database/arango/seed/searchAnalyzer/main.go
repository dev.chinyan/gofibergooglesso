package SearchAnylizerSeedMatrices

import (
	"TaskManager/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

var coll = config.ARANGO_COLLECTION

var SearchAnylizerSeedMatrices = []*ArangoDriver.ArangoSearchAnalyzerDefinition{}
