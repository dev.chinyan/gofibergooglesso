package testData

import (
	"TaskManager/src/config"
	Model "TaskManager/src/models"
)

var TaskSeedMatrix = map[string]interface{}{
	"Collection": config.TASK_COLLECTION,
	"Seed":       true,
	"Data": []interface{}{
		Model.Task{
			Id:              "tasks/84108c7d-a85f-4f4d-add2-011b44c4edd1",
			Key:             "84108c7d-a85f-4f4d-add2-011b44c4edd1",
			ProfileID:       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
			TaskName:        "Expose protected endpoints to perform CRUD on Tasks",
			TaskDescription: "After Integrate Google Single Sign, create a protected endpoint for CRUD of tasks",
			ProgressStatus:  "PENDING",
			Active:          true,
			CreatedAt:       "2023-10-29T19:56:46+08:00",
			CreatedBy:       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
			UpdatedAt:       "2023-10-29T19:56:46+08:00",
			UpdatedBy:       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
		},
	},
}
