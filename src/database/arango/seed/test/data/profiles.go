package testData

import (
	"TaskManager/src/config"
	Model "TaskManager/src/models"
)

var ProfileSeedMatrix = map[string]interface{}{
	"Collection": config.PROFILE_COLLECTION,
	"Seed":       true,
	"Data": []interface{}{
		Model.Profile{
			Id:              "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
			Key:             "12a86fc2-5284-4b56-bdc0-bfea69be70b9",
			GoogleProfileID: "109227981624876028351",
			Email:           "dev.chinyan@gmail.com",
			FamilyName:      "yan",
			GivenName:       "chin",
			CreatedAt:       "2023-10-29T19:56:46+08:00",
			CreatedBy:       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
			UpdatedAt:       "2023-10-29T19:56:46+08:00",
			UpdatedBy:       "profiles/12a86fc2-5284-4b56-bdc0-bfea69be70b9",
		},
	},
}
