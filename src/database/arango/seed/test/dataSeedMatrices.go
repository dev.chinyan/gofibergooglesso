package testSeed

import (
	testData "TaskManager/src/database/arango/seed/test/data"
)

var TestSeedMatrices = []map[string]any{
	//Collection
	testData.ProfileSeedMatrix,
	testData.TaskSeedMatrix,

	//Edge Collection
}
