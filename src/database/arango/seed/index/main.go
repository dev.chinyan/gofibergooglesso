package Indexes

import (
	"TaskManager/src/config"
)

type IndexSeedMatrices struct {
	CollectionName string
	IndexFields    []string
}

var IndexesSeedMatrices = []IndexSeedMatrices{
	{
		CollectionName: config.PROFILE_COLLECTION,
		IndexFields:    []string{"googleProfileID"},
	}, {
		CollectionName: config.TASK_COLLECTION,
		IndexFields:    []string{"googleProfileID"},
	},
}
