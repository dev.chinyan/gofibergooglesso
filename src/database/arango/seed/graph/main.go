package GraphSeedMatrices

import (
	"TaskManager/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

type GraphSeedMatric struct {
	GraphName      string
	GraphAttribute *ArangoDriver.CreateGraphOptions
}

var coll = config.ARANGO_COLLECTION
var edge = config.ARANGO_EDGE_COLLECTION

var GraphSeedMatrices = []GraphSeedMatric{}
