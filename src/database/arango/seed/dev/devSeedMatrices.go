package devSeed

import (
	devData "TaskManager/src/database/arango/seed/dev/data"
)

var TestSeedMatrices = []map[string]any{
	//Collection
	devData.ProfilesSeedMatrix,
	devData.TasksSeedMatrix,

	//Edge Collection

}
