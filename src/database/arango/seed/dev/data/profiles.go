package devData

import (
	"TaskManager/src/config"
	// "TaskManager/src/model"
)

var ProfilesSeedMatrix = map[string]interface{}{
	"Collection": config.PROFILE_COLLECTION,
	"Seed":       true,
	"Data":       []interface{}{},
}
