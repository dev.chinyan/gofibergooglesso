package devData

import (
	"TaskManager/src/config"
	// "TaskManager/src/model"
)

var TasksSeedMatrix = map[string]interface{}{
	"Collection": config.TASK_COLLECTION,
	"Seed":       true,
	"Data":       []interface{}{},
}
