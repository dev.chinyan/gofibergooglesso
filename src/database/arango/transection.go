package arango

import (
	ColorLog "TaskManager/src/helper/colorlog"

	ArangoDriver "github.com/arangodb/go-driver"
)

func UseTransection(collections ArangoDriver.TransactionCollections, callback func() (interface{}, error)) (interface{}, error) {
	db, err := GetDatabase()
	if err != nil {
		ColorLog.Red(err)
	}

	transactionID, err := db.BeginTransaction(nil, collections, nil)
	if err != nil {
		ColorLog.Red(err)
		return nil, err
	}
	defer db.CommitTransaction(nil, transactionID, nil)
	data, err := callback()
	if err != nil {
		status, err := db.TransactionStatus(nil, transactionID)
		if err != nil {
			ColorLog.Red(err)
			return nil, err
		}
		if status.Status == ArangoDriver.TransactionRunning {
			err = db.AbortTransaction(nil, transactionID, nil)
			return nil, err
		}
	}
	return data, nil

}
