package arango

func SetupArango() {
	//Create Collection If not Exist
	SetupCollection()
	//Create Graph And Edge Collection If Not Exist
	SetupGraph()

	// Truncade Collection for test
	ResetDB()

	//Create View and Search Analyzer If Not Exist
	SetupSearchAnalyzer()
	SetupView()
	//Create Func if Not Exist (DB Procedure)
	SetupFunc()
	//Seed Data
	Seed()

}
