package arango

import (
	GraphSeedMatrices "TaskManager/src/database/arango/seed/graph"
	ColorLog "TaskManager/src/helper/colorlog"
)

func SetupGraph() {
	db, err := GetDatabase()
	if err != nil {
		ColorLog.Red(err)
	}

	for _, seedMatrix := range GraphSeedMatrices.GraphSeedMatrices {
		GraphExist, err := db.GraphExists(nil, seedMatrix.GraphName)
		if err != nil {
			ColorLog.Red(err)
		}
		if !GraphExist {
			db.CreateGraphV2(nil, seedMatrix.GraphName, seedMatrix.GraphAttribute)
			ColorLog.Green(seedMatrix.GraphName + "Graph Created Successfully")
		} else {
			ColorLog.Yellow("Graph " + seedMatrix.GraphName + " Exist")
		}
	}

}
