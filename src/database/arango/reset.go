package arango

import (
	"TaskManager/src/config"
	ColorLog "TaskManager/src/helper/colorlog"
)

func ResetDB() {
	Env := config.ENV
	if Env.ENV == "test" {
		db, err := GetDatabase()
		if err != nil {
			ColorLog.Red("GET DB ERROR :", err)
		}
		if db.Name() == config.ENV.ARANGODB_DBNAME+"_test" {
			for _, val := range config.ARANGO_COLLECTION {

				collection, err := db.Collection(nil, val)
				if err != nil {
					ColorLog.Red("Collection : " + val)
					ColorLog.Red("GET COLLECTION ERROR : ", err)
				}
				truncateError := collection.Truncate(nil)
				if truncateError != nil {
					ColorLog.Red(truncateError.Error())
					continue
				}
				ColorLog.Magenta("Truncated " + val + " Collection In " + db.Name() + " Database")
			}

			for _, edg := range config.ARANGO_EDGE_COLLECTION {

				edgeCollection, err := db.Collection(nil, edg)
				if err != nil {
					ColorLog.Red("Collection : " + edg)
					ColorLog.Red("GET COLLECTION ERROR :", err)
				}
				truncateError := edgeCollection.Truncate(nil)
				if truncateError != nil {
					ColorLog.Red(truncateError.Error())
					continue
				}
				ColorLog.Magenta("Truncated " + edg + " EDGE Collection In " + Env.ARANGODB_DBNAME + "_test" + " Database")
			}
		}
	}
}
