package baseRepo

import (
	"TaskManager/src/database/arango"
	ColorLog "TaskManager/src/helper/colorlog"
	"errors"
	"fmt"
	"strings"

	ArangoDriver "github.com/arangodb/go-driver"
)

var (
	connection, connectionError = arango.GetConnection()
	client, clientError         = arango.GetClient()
	database, databaseError     = arango.GetDatabase()
)

type BaseRepository struct {
	Collection string
}

func ErrorHandling() error {
	if connectionError != nil {
		return connectionError
	}
	if clientError != nil {
		return clientError
	}
	if databaseError != nil {
		return databaseError
	}
	return nil
}

func (repo *BaseRepository) Insert(doc any) error {
	err := ErrorHandling()
	if err != nil {
		return err
	}

	//Get Collection
	collection, err := database.Collection(nil, repo.Collection)
	if err != nil {
		errMsg := "failed to open " + repo.Collection + " collection "
		return errors.New(errMsg)
	}

	//Insert
	_, err = collection.CreateDocument(nil, doc)
	if err != nil {
		errMsg := "failed to create document. From " + repo.Collection + " collection "
		return errors.New(errMsg)
	}
	return nil
}

func (repo *BaseRepository) Update(docMap map[string]any, ID string) error {
	err := ErrorHandling()
	if err != nil {
		return err
	}
	// Get Collection
	collection, err := database.Collection(nil, repo.Collection)
	if err != nil {
		errMsg := "failed to open " + repo.Collection + " collection "
		return errors.New(errMsg)
	}

	// Update
	ColorLog.Yellow("Collection : ", repo.Collection)
	key := strings.Split(ID, "/")[1]
	_, err = collection.UpdateDocument(nil, key, docMap)
	if err != nil {
		errRes := "update document failed : " + fmt.Sprint(err)
		return errors.New(errRes)
	}
	return nil
}

func (repo *BaseRepository) Fetch(id string, model any) (any, error) {
	err := ErrorHandling()
	if err != nil {
		return nil, err
	}

	doc := model
	query := `return  Document(@ID)`

	cursor, err := database.Query(nil, query, map[string]interface{}{"ID": id})
	if err != nil {
		errRes := "Arango Query Malform : " + fmt.Sprint(err)
		return nil, errors.New(errRes)
	}
	defer cursor.Close()

	_, readErr := cursor.ReadDocument(nil, &doc)
	if readErr != nil {
		// handle other errors
		errRes := fmt.Sprint(readErr)
		return nil, errors.New(errRes)
	}
	return doc, nil
}

func (repo *BaseRepository) Delete(_key string) error {

	err := ErrorHandling()
	if err != nil {
		return err
	}
	collection, err := database.Collection(nil, repo.Collection)
	if err != nil {
		errRes := "failed to open " + repo.Collection + " collection "
		return errors.New(errRes)
	}
	_, err = collection.RemoveDocument(nil, _key)
	if err != nil {
		errRes := "failed to remove document(" + _key + ") from " + repo.Collection + " collection "
		return errors.New(errRes)
	}
	return nil
}

func (repo *BaseRepository) FindManyByQuery(query string, bindVars map[string]any) ([]map[string]any, error) {
	err := ErrorHandling()
	if err != nil {
		return nil, err
	}

	cursor, err := database.Query(nil, query, bindVars)
	if err != nil {
		return nil, err
	}
	defer cursor.Close()

	var doc map[string]any
	var docs []map[string]any

	for {
		_, err = cursor.ReadDocument(nil, &doc)
		if ArangoDriver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			ColorLog.Red("Query Exercute Error : ", err)
			break
		} else {
			if doc != nil {
				docs = append(docs, doc)
			}
		}
	}

	return docs, nil
}

func (repo *BaseRepository) ExercuteQuery(query string, bindVars map[string]any) error {
	err := ErrorHandling()
	if err != nil {
		return err
	}

	cursor, err := database.Query(nil, query, bindVars)
	if err != nil {
		return err
	}
	defer cursor.Close()
	return nil
}
