package repository

import (
	config "TaskManager/src/config"
	baseRepo "TaskManager/src/database"
	ColorLog "TaskManager/src/helper/colorlog"
	docHelper "TaskManager/src/helper/document"
	Model "TaskManager/src/models"
)

type ProfileRepository struct {
	*baseRepo.BaseRepository
}

//create instance
var ProfileRepo = &ProfileRepository{
	&baseRepo.BaseRepository{
		Collection: config.PROFILE_COLLECTION,
	},
}

func (repo ProfileRepository) FindByGoogleProfileID(googleProfileID string) ([]*Model.Profile, error) {
	query := `
		FOR p IN @@profiles 
		FILTER p.googleProfileID == @googleProfileID 
		return p
	`
	bindParams := map[string]any{
		"@profiles":       config.PROFILE_COLLECTION,
		"googleProfileID": googleProfileID,
	}
	mapRes, err := repo.FindManyByQuery(query, bindParams)
	if err != nil {
		ColorLog.Yellow(err)
		return nil, err
	}

	res, err := docHelper.ParseDocs[*Model.Profile](mapRes)
	if err != nil {
		return nil, err
	}

	return res, nil
}
