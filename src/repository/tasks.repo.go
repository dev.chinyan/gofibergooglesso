package repository

import (
	config "TaskManager/src/config"
	baseRepo "TaskManager/src/database"
	ColorLog "TaskManager/src/helper/colorlog"
	docHelper "TaskManager/src/helper/document"
	Model "TaskManager/src/models"
)

type TaskRepository struct {
	*baseRepo.BaseRepository
}

// create instance
var TaskRepo = &TaskRepository{
	&baseRepo.BaseRepository{
		Collection: config.TASK_COLLECTION,
	},
}

func (repo TaskRepository) List(profileID, createdBy string, offset, limit int) ([]*Model.Task, error) {
	query := `
		FOR t IN @@tasks 
		FILTER t.profileID == @profileID || t.createdBy == @createdBy 
		LIMIT @offset, @limit
		return t
	`
	bindParams := map[string]any{
		"@tasks":    config.TASK_COLLECTION,
		"profileID": profileID,
		"createdBy": createdBy,
		"offset":    offset,
		"limit":     limit,
	}
	mapRes, err := repo.FindManyByQuery(query, bindParams)
	if err != nil {
		ColorLog.Yellow(err)
		return nil, err
	}

	res, err := docHelper.ParseDocs[*Model.Task](mapRes)
	if err != nil {
		return nil, err
	}

	return res, nil
}
