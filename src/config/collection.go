package config

//Enum

const (
	//Collection
	PROFILE_COLLECTION = "profiles"
	TASK_COLLECTION    = "tasks"

	//Edge Collection
	// TASK_ASSIGNMENT_COLLECTION = "task_assignment_edge"

)

//FOR arango collection auto creation
var ARANGO_COLLECTION = [...]string{
	PROFILE_COLLECTION,
	TASK_COLLECTION,
}

var ARANGO_EDGE_COLLECTION = [...]string{
	// TASK_ASSIGNMENT_COLLECTION,
}
