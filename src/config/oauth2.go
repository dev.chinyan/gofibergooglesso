package config

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var ENV = GetEnv()

func SetupGoogleOauth2Config() *oauth2.Config {
	conf := &oauth2.Config{
		ClientID:     ENV.GOOGLE_CONSOLE_CLIENT_ID,
		ClientSecret: ENV.GOOGLE_CONSOLE_CLIENT_SECRET,
		RedirectURL:  "http://127.0.0.1:9090/auth/google/callback",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}
	return conf
}
