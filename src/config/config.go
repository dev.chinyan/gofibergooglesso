package config

import (
	ColorLog "TaskManager/src/helper/colorlog"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

type Configuration struct {
	HOST string `mapstructure:"HOST"`
	PORT string `mapstructure:"PORT"`
	ENV  string `mapstructure:"ENV"`

	ARANGODB_PROTOCOL string `mapstructure:"ARANGODB_PROTOCOL"`
	ARANGODB_HOST     string `mapstructure:"ARANGODB_HOST"`
	ARANGODB_PORT     string `mapstructure:"ARANGODB_PORT"`
	ARANGODB_USER     string `mapstructure:"ARANGODB_USER"`
	ARANGODB_PASSWORD string `mapstructure:"ARANGODB_PASSWORD"`
	ARANGODB_DBNAME   string `mapstructure:"ARANGODB_DBNAME"`

	JWT_SECRET string `mapstructure:"JWT_SECRET"`

	GOOGLE_CONSOLE_APP_NAME      string `mapstructure:"GOOGLE_CONSOLE_APP_NAME"`
	GOOGLE_CONSOLE_SUPPORT_EMAIL string `mapstructure:"GOOGLE_CONSOLE_SUPPORT_EMAIL"`
	GOOGLE_CONSOLE_CLIENT_ID     string `mapstructure:"GOOGLE_CONSOLE_CLIENT_ID"`
	GOOGLE_CONSOLE_CLIENT_SECRET string `mapstructure:"GOOGLE_CONSOLE_CLIENT_SECRET"`
	GOOGLE_CONSOLE_AUTH_URI      string `mapstructure:"GOOGLE_CONSOLE_AUTH_URI"`
	GOOGLE_CONSOLE_TOKEN_URI     string `mapstructure:"GOOGLE_CONSOLE_TOKEN_URI"`
}

//Instance
var env Configuration

var (
	workingDir, _ = os.Getwd()
	envPath       = filepath.Dir(workingDir) + "/" + filepath.Base(workingDir) //+ "/.env"
	// conf, _       = getConfig(envPath, "dev")
	conf, _ = getConfig(envPath, ".env")
	// testConf, _ = getConfig(envPath, "test")
)

func getConfig(path, env_name string) (*Configuration, error) {
	path = strings.Split(path, "/src")[0] // This is for test since it start from api dir
	// ColorLog.Cyan("path : ", path)
	config := &Configuration{}
	viper.AddConfigPath(path)
	viper.SetConfigName(env_name)
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		ColorLog.Red(err)
		return nil, err
	}

	err = viper.Unmarshal(config)
	if err != nil {
		ColorLog.Red(err)
		return nil, err
	}
	// ColorLog.Magenta("Config : ", config)
	return config, nil

}

func GetEnv() Configuration {
	env = *conf
	// ColorLog.Magenta("APP_ENV : ", os.Getenv("APP_ENV"))
	// if os.Getenv("APP_ENV") == "test" {
	// 	env = *testConf
	// }
	return env
}

// func GetTestEnv() Configuration {
// 	return *testConf
// }
