# GOOGLE SINGLE SIGN-ON OAUTH2
>#### Tech Stack
> - golang, fiber framework
> - database: ArangoDB
> - validator: go-playground

# Documentation
>#### INSTALLATION and SETUP
>```bash
>    go mod download
>```

>#### BUILD Application
>```bash
>    go build -o ${APP_NAME}
>```

>#### START Built Application
>```bash
>    ./${APP_NAME}
>```

>#### START Dev Application
>```bash
>    go run main.go
>```

>#### START DB and Application
>```bash
>    docker-compose up -d
>```

#### RUN a REGRESSION TEST on src/api folder using mock server
```bash
    docker-compose up -d
    go clean -testcache
	ENV=test go test -v ./src/api/...
```