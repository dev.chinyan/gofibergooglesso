package main

import (
	server "TaskManager/src"

	"TaskManager/src/config"

	// _ "TaskManager/docs"

	"github.com/gofiber/fiber/v2"
)

func main() {
	conf := config.GetEnv()
	Server := &server.Server{
		Host:       conf.HOST,
		Port:       conf.PORT,
		ProjectEnv: conf.ENV,
		Engine: fiber.New(fiber.Config{
			Prefork:       true,
			CaseSensitive: true,
			StrictRouting: true,
			ServerHeader:  "Fiber",
			AppName:       "Task Management App v1.0.1",
		}),
	}
	Server.Setup()
	Server.Start()
}
