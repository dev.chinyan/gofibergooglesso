startdb:
	docker-compose up -d

stopdb:
	docker-compose down

start:
	go run main.go

resetDB:
	ENV=Test go run main.go resetDB

test:
	go clean -testcache
	ENV=test go test -v ./src/api/...

build:
	go build .

dockerNetwork:
	docker network create backend_network