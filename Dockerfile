FROM golang:1.21.3-alpine

WORKDIR /TaskManager

# Download necessary Go modules
COPY go.mod ./
# COPY go.sum ./
#RUN go mod download
RUN go mod tidy

#copy source file
COPY . .

#build
RUN go build .

# RUN go build -o /TaskManager

RUN ls -a

RUN chmod +x TaskManager

EXPOSE 9090

CMD ["./TaskManager"]